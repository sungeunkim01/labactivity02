package code.hellopackage;

import code.secondpackage.Utilities;
import java.util.Scanner;
import java.util.Random;

public class Greeter {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Random rand = new Random();

        // Ask user to enter an integer number and store it in a variable, and
        // prints the result to make sure.
        System.out.println("Enter a integer number: ");
        int intNum = sc.nextInt();
        System.out.println("Entered number is: " + intNum);

        //Using doubleMe method from Utilities class, pass intNum to double it.
        int doubleNum = Utilities.doubleMe(intNum);
        System.out.println("Doubled result: " + doubleNum);
    }
}